1. При запуске приложения отображаются следующие логи:

2024-02-28 00:39:48.624 10596-10596 MainActivity            com.example.activity                 D  test log create

2024-02-28 00:39:48.680 10596-10596 MainActivity            com.example.activity                 D  test log start

2024-02-28 00:39:48.704 10596-10596 MainActivity            com.example.activity                 D  test log resume


Сначала вызывается метод onCreate(), который подготавливает макет, затем с помощью метода onStart() приложение отображается, но недоступно для взаимодействия пользователю, заключительным методом является onResume(), которое позволяет пользователя взаимодействовать с компонентами

--------------------

2. При повороте экрана телефона отображаются следующие логи:

2024-02-28 00:46:38.667 10596-10596 MainActivity            com.example.activity                 D  test log pause

2024-02-28 00:46:38.682 10596-10596 MainActivity            com.example.activity                 D  test log stop

2024-02-28 00:46:38.699 10596-10596 MainActivity            com.example.activity                 D  test log destroy

2024-02-28 00:46:38.802 10596-10596 MainActivity            com.example.activity                 D  test log create

2024-02-28 00:46:38.812 10596-10596 MainActivity            com.example.activity                 D  test log start

2024-02-28 00:46:38.817 10596-10596 MainActivity            com.example.activity                 D  test log resume


Сначала вызывается метод onPause(), который не позволяет пользователю взаимодействовать с компонентами приложения, затем метод onStop(), который прерывает отображение приложения, после метод onDestroy(), который уничтожает приложение. После выполнения поворота приложение запускается вновь.

---------------------

3. При выходе из приложения отображаются следующие логи:

2024-02-28 00:53:58.775 10596-10596 MainActivity            com.example.activity                 D  test log pause

2024-02-28 00:53:59.959 10596-10596 MainActivity            com.example.activity                 D  test log stop


---------------------

4. При возврате в приложение отображаются следующие логи:

2024-02-28 00:55:19.072 10596-10596 MainActivity            com.example.activity                 D  test log restart

2024-02-28 00:55:19.081 10596-10596 MainActivity            com.example.activity                 D  test log start

2024-02-28 00:55:19.091 10596-10596 MainActivity            com.example.activity                 D  test log resume


Так как на предыдущем шаге не был вызван метод onDestroy(), то после возврата в приложение вызывается метод onRestart(), который восстанавливает предыдущее состояние приложения

---------------------

5. При добавлении finish() отображаются следующие логи:

2024-02-28 01:02:32.809 11170-11170 MainActivity            com.example.activity                 D  test log create

2024-02-28 01:02:36.991 11170-11170 MainActivity            com.example.activity                 D  test log destroy


То есть был вызван метод onCreate(), а затем уничтожилось методом onDestroy()
