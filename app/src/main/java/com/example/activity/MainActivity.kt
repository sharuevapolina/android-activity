package com.example.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Email
import androidx.compose.material.icons.rounded.Phone
import androidx.compose.material.icons.rounded.Share
import androidx.compose.material3.Icon
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.activity.ui.theme.ActivityTheme
import android.util.Log


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ActivityTheme {
                Surface(
                    color = Color(0xFFD2DCE8),
                    modifier = Modifier.fillMaxSize()

                ) {
                    BusinessCard()
                }
            }
        }

        Log.d(getString(R.string.main_log), "test log create")

        //finish()
    }

    override fun onStart() {
        super.onStart()
        Log.d(getString(R.string.main_log), "test log start")
    }

    override fun onResume() {
        super.onResume()
        Log.d(getString(R.string.main_log), "test log resume")
    }

    override fun onPause() {
        super.onPause()
        Log.d(getString(R.string.main_log), "test log pause")
    }

    override fun onStop() {
        super.onStop()
        Log.d(getString(R.string.main_log), "test log stop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(getString(R.string.main_log), "test log destroy")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d(getString(R.string.main_log), "test log restart")
    }
}

@Composable
fun BusinessCard() {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        val image = painterResource(R.drawable.logo)
        Image(
            painter = image,
            contentDescription = null,
            Modifier
                .fillMaxHeight(0.4f)
            )
        Text(
            text = stringResource(R.string.full_name),
            fontSize = 40.sp,
            color = Color.Black,
        )
        Text(
            text = stringResource(R.string.title),
            fontSize = 30.sp,
            color = Color.Black,
        )
        Spacer(
            modifier = Modifier.padding(bottom = 100.dp)
        )
        ContactRow(
            text = stringResource(R.string.phone),
            icon = Icons.Rounded.Phone
        )
        ContactRow(
            text = stringResource(R.string.social_media),
            icon = Icons.Rounded.Share
        )
        ContactRow(
            text = stringResource(R.string.email),
            icon = Icons.Rounded.Email
        )
    }
}

@Composable
fun ContactRow(
    text: String, icon: ImageVector
) {
    Row(
        horizontalArrangement = Arrangement.Center,
        modifier = Modifier.padding(20.dp)
    ) {
        Icon(
            imageVector = icon,
            contentDescription = null,
            tint = Color(0xFF00356D),
            modifier = Modifier.weight(1f)
        )
        Text(
            text = text,
            color = Color.Black,
            fontSize = 20.sp,
            modifier = Modifier.weight(3f)


        )
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun DefaultPreview() {
    Surface(
        modifier = Modifier.fillMaxSize(), color = Color(0xFFD2E8D4)
    ) {
        ActivityTheme {
            BusinessCard()
        }
    }
}